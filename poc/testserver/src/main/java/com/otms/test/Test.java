package com.otms.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test {
	
	static final Logger logger = LoggerFactory.getLogger(Test.class);
	
	public static void main(String[] args) {
	    logger.info("entry the main method");
		
		ITestService testService = new TestService();
		testService.sayHello("javaagent");
		
		testService.sayHello2("atom", 34, "welcome to my home");
		
	}
}
