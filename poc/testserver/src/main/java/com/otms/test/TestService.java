package com.otms.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestService implements ITestService {
    
    static final Logger logger = LoggerFactory.getLogger(TestService.class);
    
    public void sayHello(String name) {
        logger.info("entry method sayHello");
        logger.info("name = {} say hello to you !", name);
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            logger.error("get Exception when sayHello, details are", e);
        }
    }

    public void sayHello2(String name, Integer age, String word) {
        logger.info("entry method sayHello2");
        logger.info("user name : {}, age : {} , say the word {} to world ", new Object[]{name, age, word});
        try {
            Thread.sleep(500);
        } catch (Exception e) {
            logger.error("get Exception when sayHello2, details are", e);
        }
        
        
        
        
    }

}
