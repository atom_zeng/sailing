package com.test.agent;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MonitoringTransformer implements ClassFileTransformer {

    final static Logger logger = LoggerFactory.getLogger(MonitoringTransformer.class);

    final static String startTime = "\nlong startTime = System.currentTimeMillis();\n";
    final static String endTime = "\nlong endTime = System.currentTimeMillis();\n";
    // final static String loggerStr =
    // "\n Logger loggerInAgent = LoggerFactory.getLogger({0}.class);\n";
    final static String outputStr = "\nlogger.info(\"this method : {0} cost : \"+ (endTime - startTime));\n";

    final static String importLogger = "\n import org.slf4j.Logger;\n";
    final static String importLoggerFactory = "\nimport org.slf4j.LoggerFactory;\n";

    private List<String> classList = new ArrayList<>();
    /**
     * key is className, values is methodList
     */
    private Map<String, List<String>> methodMap = new HashMap<>();

    private static IMonitorConfig monitorConfig = null;

    // static {
    //
    // classList.add("com.otms.test.TestService");
    // methodList.add("com.otms.test.TestService.sayHello");
    //
    // }

    public MonitoringTransformer() {
    }

    public MonitoringTransformer(String configPath) {
        monitorConfig = MonitorConfig.getInstance(configPath);
        initParamerByConfig();
    }

    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
            ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        logger.info("entry transform, the classname is {} ", className);
        byte[] result = null;
        // use list to store byte[]
        List<Byte> tempList = new ArrayList<>();
        for (String classNameInConfig : classList) {
            logger.info("entry the loop in classList, classNameInConfig is {}", classNameInConfig);
            if (className.startsWith(classNameInConfig)) {
                className = className.replace("/", ".");
                logger.info("after replace the real classname is {}", className);
                CtClass ctClass = null;
                List<String> methodList = methodMap.get(classNameInConfig);
                try {
                    // javaassit class

                    ctClass = ClassPool.getDefault().get(className);
                    // reflect class couldnot get class again will trigger the
                    // exception LinkageError
                    // Class<?> clazz = Class.forName(className);
                    /**
                     * Exception in thread "main" java.lang.LinkageError: loader
                     * (instance of sun/misc/Launcher$AppClassLoader): attempted
                     * duplicate class definition for name:
                     * "com/otms/test/TestService" at
                     * java.lang.ClassLoader.defineClass1(Native Method)
                     */

                    logger.info("CtMethod[] details are {}", printArray("CtMethod", ctClass.getDeclaredMethods()));

                    for (String methodName : methodList) {
                        logger.info("entry loop the mthodList , methodName is {}", methodName);
                        // get method name, parameter type by javaassit
                        CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
                        CtClass[] parameterTypes = ctMethod.getParameterTypes();
                        logger.info("the parameterTypes from javaassit are {}", printArray("CtClass", parameterTypes));

                        String newMethodName = methodName + "$agent";

                        ctMethod.setName(newMethodName);
                        CtMethod copyMethod = CtNewMethod.copy(ctMethod, methodName, ctClass, null);

                        StringBuilder stringBuilder = new StringBuilder();
                        // append the agent code to get time cost and get params
                        // stringBuilder
                        // .append(importLogger)
                        // .append(importLoggerFactory);
                        stringBuilder.append("{")
                        // .append(MessageFormat.format(loggerStr, className))
                                .append(startTime);

                        // print the parameters value

                        StringBuilder paramArray = new StringBuilder();
                        for (int i = 0; i < parameterTypes.length; i++) {
                            paramArray.append("$" + (i + 1)).append(",");
                        }
                        if (0 != parameterTypes.length) {
                            paramArray.deleteCharAt(paramArray.lastIndexOf(","));
                        }
                        // append the paramObjects in the agent class

                        stringBuilder.append("\n Object[] params = ").append("new Object[]{").append(paramArray)
                                .append("};");

                        stringBuilder.append("\nlogger.info(\"param[0] is {}\", params[0]);\n").append(newMethodName)
                                .append("($$);\n").append(endTime)
                                .append(MessageFormat.format(outputStr, newMethodName)).append("}");
                        logger.info("----------------------------------------");
                        logger.info("new method body is {}", stringBuilder.toString());
                        logger.info("----------------------------------------");
                        copyMethod.setBody(stringBuilder.toString());
                        ctClass.addMethod(copyMethod);
                    }
                    // combine all byte[] together
                    if (null == result) {
                        result = ctClass.toBytecode();
                    } else {
                        result = appendData(result, ctClass.toBytecode());
                    }
                } catch (Exception e) {
                    logger.error("got Exception ,details as ", e);
                }
            }
        }
        return result;
    }

    /**
     * combine the two byte[]
     * 
     * @param firstObject
     * @param secondObject
     * @return
     */
    private byte[] appendData(byte[] firstObject, byte[] secondObject) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            if (firstObject != null && firstObject.length != 0)
                outputStream.write(firstObject);
            if (secondObject != null && secondObject.length != 0)
                outputStream.write(secondObject);
        } catch (IOException e) {
            logger.error("failed to appendData , exception details are ", e);
        }
        return outputStream.toByteArray();
    }

    public void initParamerByConfig() {
        String classNameString = monitorConfig.getStringValue(monitorConfig.CLASSNAME, null);
        logger.info("entry initParamerByConfig the classNameString is {}", classNameString);
        loadConfigByArray(classNameString.split(";"));
    }

    /**
     * sample : com/otms/test/TestService:sayHello,sayHello2;
     * 
     * @param classNameArray
     * 
     *            will init the data in classList and methodMap
     * 
     * @return
     */
    private void loadConfigByArray(String[] classNameArray) {
        if (!classList.isEmpty() && !methodMap.isEmpty()) {
            logger.info("ignore loadConfigByArray, avoid init twice!");
            return;
        }
        if(null == classNameArray || classNameArray.length==0 ){
            logger.info("ignore loadConfigByArray, null == classNameArray || classNameArray.length==0!");
            return;
        }
        logger.info("the classNameArray's size is {}, the details are {} ", 
                new Object[]{classNameArray.length, printArray(null, classNameArray)});
        try {
            for (String classAndMethodName : classNameArray) {
                // inert the class name into classList
                if (null != classAndMethodName && !"".equals(classAndMethodName)) {
                    String[] classAndMethodNameArray = classAndMethodName.split(":");
                    logger.info("after classAndMethodName split the classAndMethodNameArray's  is {}",
                            printArray(null, classAndMethodNameArray));
                    System.out.println(classAndMethodNameArray.length);
                    if (2 > classAndMethodNameArray.length) {
                        throw new RuntimeException(
                                "failed to loadConfigByArray, the classAndMethodNameArray.length < 2");
                    }
                    String classNameInConfig = classAndMethodNameArray[0];
                    String methodNamesInConifg = classAndMethodNameArray[1];
                    logger.info("in loadConfigByArray, the classNameInConfig is {}, " + "methodNameInConifg is {}",
                            new Object[] { classNameInConfig, methodNamesInConifg });
                    classList.add(classNameInConfig);
                    String[] methodNamesArray = methodNamesInConifg.split(",");
                    loadMethodMap(classNameInConfig, methodNamesArray);
                }
            }
        } catch (Exception e) {
            logger.error("failed to loadConfigByArray the details is {}", e);
        }
    }

    private void loadMethodMap(String className, String[] methodNames) {
        logger.info("entry loadMethodMap, className is {}, the methodNames is {}", new Object[] { className,
                printArrayToString(methodNames) });
        List<String> tempList = methodMap.get(className);
        if (null == tempList) {
            tempList = new ArrayList<>();
        }
        try {
            for (String methodName : methodNames) {
                logger.info("in loadMethodMap, insert the methodname {} into tempList", methodName);
                tempList.add(methodName);
            }

        } catch (Exception e) {
            logger.error("failed to load method map , details is ", e);
        }
        methodMap.put(className, tempList);

    }

    private String printArrayToString(String[] methodNames) {
        StringBuilder sBuilder = new StringBuilder();
        for (String methodName : methodNames) {
            sBuilder.append(methodName).append(",");
        }
        if (methodNames.length > 0) {
            sBuilder.deleteCharAt(sBuilder.lastIndexOf(","));
        }
        return sBuilder.toString();
    }

    String printArray(String type, Object[] objects) {
        StringBuilder sBuilder = new StringBuilder();

        for (Object obj : objects) {
            if ("clazz".equals(type)) {
                sBuilder.append(((Class<?>) obj).getName()).append("\n");
            } else if ("CtClass".equals(type)) {
                sBuilder.append(((CtClass) obj).getName()).append("\n");
            } else if ("CtMethod".equals(type)) {
                sBuilder.append(((CtMethod) obj).getName()).append("\n");
            } else {
                sBuilder.append(obj).append("\n");
            }

        }

        return sBuilder.toString();

    }

}
