package com.test.agent;

public interface IMonitorConfig {
    
    static final String CLASSNAME = "agent.name";

    public String getStringValue(String key, String defaultValue);
    
    public Boolean getBooleanValue(String key, Boolean defaultValue);
    
    public Integer getIntegerValue(String key, Integer defaultValue);
    
}
