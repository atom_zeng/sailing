package com.test.agent;

import java.lang.instrument.Instrumentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAgent {
    
    static final Logger logger = LoggerFactory.getLogger(MyAgent.class);
    
    public static void premain(String agentOps){
        logger.info("entry premain with one param");
        
    }
	
	public static void premain(String agentOps, Instrumentation inst){
		logger.info("entry premain with String agentOps, Instrumentation inst params, the agentOps is {}", agentOps);
//		inst.addTransformer(new MonitorTransformerimplements());
		inst.addTransformer(new MonitoringTransformer(agentOps));
	}
	
	
}
