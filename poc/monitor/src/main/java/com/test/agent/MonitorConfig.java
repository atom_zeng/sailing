package com.test.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * TODO comments here 
 * 
 * @author atomyuansheng
 *
 */
public class MonitorConfig implements IMonitorConfig {
    
    static final Logger logger = LoggerFactory.getLogger(MonitorConfig.class);
    
    
    private static Properties prop = null;
    
    private static IMonitorConfig instance = null;
    
    private static Object lock = new Object();
    
    
    private MonitorConfig(String path) throws IOException{
        prop = new Properties();
        File configFile = new File(path);
        prop.load(new FileInputStream(configFile));
    }
    

    /*
     * the singleton to get IMonitorConfig
     * TODO 
     * @see com.test.agent.IMonitorConfig#getInstance(java.lang.String)
     */
    public static IMonitorConfig getInstance(String path) {
        if(null == instance){
            synchronized (lock) {
                try {
                    instance = new MonitorConfig(path);
                    logger.info("init the MonitorConfig with the path in {}", path);
                } catch (IOException e) {
                    logger.error("failed to finde the config file path in {}", path);
                    logger.error("failed to finde the config exception details is ", e);
                }
            }
        }
        return instance;
    }

;    public String getStringValue(String key, String defaultValue) {
        return prop.getProperty(key, defaultValue);
    }

    public Boolean getBooleanValue(String key, Boolean defaultValue) {
        String value = prop.getProperty(key, null);
        return (null != value) ? Boolean.valueOf(value) : defaultValue;
    }

    public Integer getIntegerValue(String key, Integer defaultValue) {
        String value = prop.getProperty(key, null);
        return  (null != value) ? Integer.valueOf(value) : defaultValue;
    }




}
