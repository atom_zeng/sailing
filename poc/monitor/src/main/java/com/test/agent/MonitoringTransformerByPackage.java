package com.test.agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;

public class MonitoringTransformerByPackage implements ClassFileTransformer {
    
    final static String startTime = "\nlong startTime = System.currentTimeMillis();\n";
    final static String endTime = "\nlong endTime = System.currentTimeMillis();\n";
    final static String output = "\nSystem.out.println(\"this method cost:\" + (endTime - startTime) + \"ms.\");\n";
    
    final static List<String> packageList = new ArrayList<String>();
    final static List<String> methodList = new ArrayList<String>();
    static {
        packageList.add("com.otms.test");
        
    }
    
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
            ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
//        System.out.println(className);
        if(className.startsWith("com/otms/test/ITestService"))
        {
            className = className.replace("/", ".");
            CtClass ctClass = null;
            try {
                ctClass = ClassPool.getDefault().get(className);
                
                for(String method : methodList){
                    if(method.startsWith(className)){
                        String methodName = method.substring(method.lastIndexOf(".")+1, method.length());
                        System.out.println("methodName is ="+ methodName );
                        
                        System.out.println("className is "+ classBeingRedefined.getName());
                        
                        String outputStr ="\nSystem.out.println(\"this method "+methodName+" cost:\" +(endTime - startTime) +\"ms.\");";  
                        CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
                        String newMethodName = methodName + "$impl";
                        ctMethod.setName(newMethodName);
                        
                        CtMethod copyMethod = CtNewMethod.copy(ctMethod, methodName, ctClass, null);
                        
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("{")
                        .append(startTime)
                        .append(newMethodName).append("($$);\n")
                        .append(endTime)
                        .append(outputStr)
                        .append("}");
                        System.out.println("new method body is "+ stringBuilder.toString());
                        copyMethod.setBody(stringBuilder.toString());
                        ctClass.addMethod(copyMethod);
                        return ctClass.toBytecode();
                    }
                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
